﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sporamic
{
    interface IRequestLocker
    {
        void StartRequest(string uniqueId);

        void FinishRequest(string uniqueId);

        bool CanProceed(string uniqueId);
    }
}
