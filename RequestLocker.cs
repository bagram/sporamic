﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sporamic
{
    public class RequestLocker : IRequestLocker
    {
        private int _totalRequests;

        private object _lockObject;

        private readonly IDictionary<string, object> _requestContainer;

        private readonly int _maxRequest;

        private bool _isGlobalLocked;

        public RequestLocker(int maxRequest)
        {
            this._maxRequest = maxRequest;
            this._lockObject = new object();
            this._requestContainer = new Dictionary<string, object>();
            this._totalRequests = 0;
            this._isGlobalLocked = false;
        }

        public void StartRequest(string uniqueId)
        {
            if (this._maxRequest != 0 && this._totalRequests == this._maxRequest)
            {
                System.Threading.Monitor.Enter(this._lockObject);
                this._isGlobalLocked = true;
            }

            object uidLock;
            lock (this._requestContainer)
            {
                if (this.Exists(uniqueId) == false)
                {
                    uidLock = new object();
                }
                else
                {
                    uidLock = this._requestContainer[uniqueId];
                }

                this._requestContainer.Add(uniqueId, uidLock);
            }

            System.Threading.Monitor.Enter(uidLock);
            _totalRequests++;
        }

        public void FinishRequest(string uniqueId)
        {
            lock (_requestContainer)
            {
                if (this.Exists(uniqueId)) {
                    var uidObjectLock = this._requestContainer[uniqueId];
                    this._requestContainer.Remove(uniqueId);
                    System.Threading.Monitor.Exit(uidObjectLock);
                    _totalRequests--;
                }
            }

            if (this._maxRequest != 0 && this._totalRequests == this._maxRequest)
            {
                try
                {
                    if (this._isGlobalLocked)
                    {
                        this._isGlobalLocked = false;
                        System.Threading.Monitor.Exit(this._lockObject);
                    }
                }
                catch
                {
                }
            }
        }

        public bool CanProceed(string uniqueId)
        {
            lock (this._requestContainer)
            {
                return this.Exists(uniqueId) == false;
            }
        }

        private bool Exists(string uniqueId)
        {
            return this._requestContainer.ContainsKey(uniqueId);
        }
    }
}
